variable "aws_access_key" {
  default     = "REPLACEME"
}

variable "aws_secret_key" {
  default     = "REPLACEME"
}

variable "aws_region" {
  default     = "us-east-1"
}

variable "aws_ami" {
  default     = "ami-9b3d128d"
}

variable "aws_setup_wekan_path" {
  default     = "/tmp/setup_wekan.sh"
}
