#!/bin/bash

# install mongo
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt update
sudo apt install -y mongodb-org mongodb-org-server mongodb-org-shell mongodb-org-mongos mongodb-org-tools
sudo systemctl start mongod
sudo systemctl enable mongod

# install meteor
curl https://install.meteor.com/ | sh

# install and build Wekan
sudo rm -rf /usr/local/lib/node_modules
sudo rm -rf ~/.npm
git clone https://github.com/wekan/wekan
cd wekan/packages
git clone https://github.com/wekan/flow-router.git kadira-flow-router
git clone https://github.com/meteor-useraccounts/core.git meteor-useraccounts-core
sed -i 's/api\.versionsFrom/\/\/api.versionsFrom/' ~/.meteor/packages/meteor-useraccounts-core/package.js
cd ..
rm package-lock.json
sudo apt install build-essential g++ capnproto nodejs nodejs-legacy npm git curl
sudo npm -g install n
npm install

cd ~/repos/wekan/.build/bundle
export MONGO_URL='mongodb://127.0.0.1:27017/admin'
export ROOT_URL='http://localhost:80'
export MAIL_URL='smtp://user:pass@mailserver.example.com:25/'
export PORT=80
node main.js

